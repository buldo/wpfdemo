﻿using System.Collections.Generic;

namespace WpfDemo.Models
{
    public static class SectionsSource
    {
        public static IEnumerable<SlabSection> GetSections()
        {
            return new[]
            {
                new SlabSection(1, 2),
                new SlabSection(2, 3),
                new SlabSection(3, 4),
                new SlabSection(4, 5),
                new SlabSection(5, 6),
                new SlabSection(6, 7),
            };
        }
    }
}