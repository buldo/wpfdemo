﻿namespace WpfDemo.Models
{
    public class SlabSection
    {
        // public SectionContour SelectionContour { get; }
        // public Hole[] Holes { get; }
        public int Elevation { get; }
        public int Depth { get; }

        public SlabSection(
            // SectionContour selectionContour,
            // Hole[] holes,
            int elevation,
            int depth)
        {
            //SelectionContour = selectionContour;
            //Holes = holes;
            Elevation = elevation;
            Depth = depth;
        }

        //Todo fix
        //        public bool IsPointInside(InternalPoint2D internalPoint2D)
        //        {
        //            if (!CheckPoint(internalPoint2D, SelectionContour)) return false;
        //            return Holes.All(hole => !CheckPoint(internalPoint2D, hole));
        //        }

        //        private static bool CheckPoint(InternalPoint2D internalPoint2D, Contour contour)
        //        {
        //            bool insidePoly = false;
        //            List<InternalPoint2D> points = contour.GetPoints();
        //            int s = points.Count - 2;
        //
        //            for (int f = 0; f < points.Count - 1; f++)
        //            {
        //                if (points[f].Y < internalPoint2D.Y && points[s].Y >= internalPoint2D.Y ||
        //                    points[s].Y < internalPoint2D.Y && points[f].Y >= internalPoint2D.Y)
        //                {
        //                    if (points[f].X + (internalPoint2D.Y - points[f].Y) / (points[s].Y - points[f].Y) *
        //                        (points[s].X - points[f].X) < internalPoint2D.X)
        //                    {
        //                        insidePoly = !insidePoly;
        //                    }
        //                }
        //
        //                s = f;
        //            }
        //
        //            return insidePoly;
        //        }
    }
}