﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Input;
using Prism.Commands;
using Prism.Mvvm;
using WpfDemo.Models;

namespace WpfDemo.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private readonly Dictionary<int, SlabSection> _idToSections = new Dictionary<int, SlabSection>();

        public ObservableCollection<SlabSectionViewModel> Sections { get; } = new ObservableCollection<SlabSectionViewModel>();

        public bool DialogResult { get; private set; }

        public ICommand OkCommand { get; }
        public ICommand PlusCommand { get; set; }

        // Only for designer
        internal MainWindowViewModel()
        {
            OkCommand = new DelegateCommand<IClosable>(ExecuteMethod);
            PlusCommand = new DelegateCommand<IHidable>(ExecutePlusCommand);
        }

        public MainWindowViewModel(IEnumerable<SlabSection> sections)
            : this()
        {
            int id = 1;
            foreach (var section in sections)
            {
                _idToSections.Add(id, section);
                var sectionViewModel = new SlabSectionViewModel(id, section.Depth, section.Elevation);
                sectionViewModel.RequestRemove += SectionViewModelOnRequestRemove;
                Sections.Add(sectionViewModel);
                id++;
            }

        }

        public IEnumerable<SlabSection> GetSelectedSections()
        {
            return _idToSections.Values.ToList();
        }

        private void SectionViewModelOnRequestRemove(object sender, EventArgs e)
        {
            var section = (SlabSectionViewModel) sender;
            section.RequestRemove -= SectionViewModelOnRequestRemove;
            Sections.Remove(section);
            _idToSections.Remove(section.Id);
        }

        private void ExecutePlusCommand(IHidable obj)
        {
            DialogResult = false;
            obj.Hide();
        }

        private void ExecuteMethod(IClosable obj)
        {
            DialogResult = true;
            obj.Close();
        }
    }
}
