﻿namespace WpfDemo
{
    public interface IClosable
    {
        void Close();
    }
}