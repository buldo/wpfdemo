﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using Prism.Mvvm;

namespace WpfDemo.ViewModels
{
    public class SlabSectionViewModel : BindableBase
    {
        public int Id { get; }
        public int Depth { get; }
        public int Elevation { get; }
        public ICommand RemoveCommand { get; }

        public SlabSectionViewModel(int id, int depth, int elevation)
        {
            Id = id;
            Depth = depth;
            Elevation = elevation;

            RemoveCommand = new DelegateCommand(ExecuteRemoveCommand);
        }

        public event EventHandler<EventArgs> RequestRemove;

        private void ExecuteRemoveCommand()
        {
            RequestRemove?.Invoke(this, EventArgs.Empty);
        }
    }
}
