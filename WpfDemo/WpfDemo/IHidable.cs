﻿namespace WpfDemo
{
    public interface IHidable
    {
        void Hide();
    }
}